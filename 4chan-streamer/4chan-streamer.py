#!/usr/bin/env python3
# A quick & dirty script for creating a playlist from all the media files in a 4chan thread

# Written in Python3

import sys
import subprocess
import requests
from bs4 import BeautifulSoup

PLAYLIST = "playlist.m3u"

def get_input():
    args = sys.argv
    if len(args) != 4 or "-p" not in args:
        print_help()
        sys.exit()
    try:
        url = args[1]
        player = args[args.index("-p") + 1]
    except IndexError:
        print("No player specified after \"-p\"")
        print_help()
        sys.exit()
    else:
        return url, player
    
def get_vids(url):
    # Function for extracting urls to videos from a 4chan thread
    # Returns a list of file urls
    try:
        r = requests.get(url)
    except Exception as e:
        print("Couldn't open given url!")
        sys.exit()
    else:
        soup = BeautifulSoup(r.content, "lxml")
        divs = soup.find_all("div", "fileText")
        vids = ["https:" + div.a.attrs["href"] for div in divs] 
        return vids   

def make_playlist(vids):
    """ Create a EXTM3U playlist file
    #EXTM3U
    #EXTINF:length, title
    https://link_to_file.webm
    """
    with open(PLAYLIST, "w") as playlist:
        # Initialize the header of the playlist file
        playlist.write("#EXTM3U\n")
        # Fetch urls and create entries to the playlist
        total = len(vids)
        for vid in enumerate(vids):
            playlist.write("#EXTINF:-1, {0}/{1}\n".format(vid[0], total))
            playlist.write(vid[1])
            playlist.write("\n\n")
        print("Playlist file: {} created.".format(PLAYLIST))

def print_help():
    print("Usage: {} URL -p MEDIAPLAYER".format(sys.argv[0]))

if __name__ == '__main__':
    print("4chan Media Streamer")
    url, player = get_input()
    vids = get_vids(url)
   
    # If "-p pl" given as an argument, create a playlist
    if player == "pl":
        make_playlist(vids)
        sys.exit()

    try:
        #subprocess.run([player, PLAYLIST])
        vids.insert(0, player)
        subprocess.run(vids)
    except FileNotFoundError:
        print("Player: {} couldn't be found!".format(player))
    else:
        pass
